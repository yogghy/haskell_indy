module UNTESTED.Nullable where

-- import Prelude hiding (null)

class (Eq a) => Nullable a where
    nullv :: a

isNull :: (Nullable a) => a -> Bool
isNull x
    | x == nullv    = True
    | otherwise     = False

isNotNull :: (Nullable a) => a -> Bool
isNotNull x = not (isNull x)
