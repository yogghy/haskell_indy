module UNTESTED.OperatorTriplePipe2 where

import InDyModel
import Compositions

-- Pipe2, but with functions with 3 inputs -- untested in-game
operatorTriplePipe2 :: (a -> b -> c -> d) -> (a -> b -> c -> e) -> (d -> e -> f) -> (a -> b -> c -> f)
operatorTriplePipe2 = pipe (flip (pipe flipPipe (pipe flipPipe flipPipe))) (pipe (apply flipPipe (liftM2 (liftM2 ap))) flip)
triplePipe2 = operatorTriplePipe2
