module UNTESTED.OperatorListLazyListBuilder where

import InDyModel

import Data.List

--------------------------------------------------------------------------------
-- operatorListLazyListBuilder dev file
--------------------------------------------------------------------------------
-- `lazyListBuilder` is the haskell function to model the in-game function
-- by the same name.
--------------------------------------------------------------------------------
-- TODO: check that this actually does work the same way as the in-game one
--------------------------------------------------------------------------------

operatorListLazyListBuilder :: a -> (a -> a) -> [a]
operatorListLazyListBuilder x f = iterate f x -- might need to use iterate' ?

lazyListBuilder = operatorListLazyListBuilder
