module UNTESTED.OperatorListUniq where

import Data.List

--------------------------------------------------------------------------------
-- Haskell model of the in-game function by the same name.
--------------------------------------------------------------------------------

operatorListUniq :: (Eq a) => [a] -> [a]
operatorListUniq = nub
uniq :: (Eq a) => [a] -> [a]
uniq = operatorListUniq
