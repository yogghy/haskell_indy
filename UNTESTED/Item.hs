{-# LANGUAGE OverloadedStrings #-}
module UNTESTED.Item where

import UNTESTED.Nullable

import Data.Text

--------------------------------------------------------------------------------
-- WIP Haskell model for the in-game Item data type
--------------------------------------------------------------------------------
-- The other operators in here could probably get moved, but this is just a dev
-- file anyways; everything will go into `InDyModel.hs` once done.
--------------------------------------------------------------------------------

data Item = Null | Item {
    name :: Text,
    stacksize :: Int
} deriving (Eq)

instance Nullable Item where
    nullv = Null

instance Show Item where
    show i = unpack (operatorStringName i) ++ "[" ++ show (operatorItemSize i) ++ "]"

item :: Text -> Item
item n = Item n 1

operatorStringName :: Item -> Text
operatorStringName Null = Data.Text.empty
operatorStringName (Item n _) = n

operatorItemSize :: Item -> Int
operatorItemSize (Item _ s) = s
operatorItemSize Null = 0

operatorWithSize :: Item -> Int -> Item
operatorWithSize (Item n _) s = Item n s
operatorWithSize Null _ = Null
withSize = operatorWithSize
