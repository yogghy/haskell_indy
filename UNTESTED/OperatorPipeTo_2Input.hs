module UNTESTED.OperatorPipeTo_2Input where

import InDyModel
import InDyLibrary
import Compositions

import UNTESTED.OperatorExpandOperatorReturn

--------------------------------------------------------------------------------
-- operatorPipeTo_2Input dev file
--------------------------------------------------------------------------------
-- Pipes a function with two inputs into another function, where `operatorPipe`
-- would fail.
--------------------------------------------------------------------------------
-- Has passed tested using `operatorStringSubstring` in-game. Still needs to be
-- thoroughly tested in general, and tested with functions that return/require
-- other functions.
--------------------------------------------------------------------------------

-- Used to compose `operatorPipeTo_2Input`
p1 :: (a -> b -> c) -> (c -> d) -> (a -> (b -> d))
p1 = flip (pipe flipPipe flipPipe)

operatorPipeTo_2Input :: (a -> b -> c) -> (c -> d) -> (a -> b -> d)
operatorPipeTo_2Input = apply expOpRes (p1 p1 expOpRes)

pipe_2    = operatorPipeTo_2Input
pipeTo_2  = operatorPipeTo_2Input
