module UNTESTED.OperatorListGetOrDefault where

import InDyModel

import Data.List

--------------------------------------------------------------------------------
-- operatorListGetOrDefault dev file
--------------------------------------------------------------------------------
-- `listGetOrDefault` is the haskell function to model the in-game function
-- by the same name.
--------------------------------------------------------------------------------
-- TODO: rigorous testing
--------------------------------------------------------------------------------

operatorListGetOrDefault :: [a] -> Int -> a -> a
operatorListGetOrDefault l i d = if ((i >= (Data.List.length l)) || (i < 0))
    then d
    else l !! i

getOrDefault = operatorListGetOrDefault
