module UNTESTED.OperatorIntegerPower where

import InDyModel

import UNTESTED.OperatorListLazyListBuilder

--------------------------------------------------------------------------------
-- integerPower dev file
--------------------------------------------------------------------------------
-- Returns the first input to the power of the second, where the second is a
-- positive integer
--------------------------------------------------------------------------------
-- Tested in-game, but it wasn't with this exact implementation
--------------------------------------------------------------------------------

operatorMultiplication :: Double -> Double -> Double
operatorMultiplication a b = a * b
multiplication = operatorMultiplication

powerList :: Double -> [Double]
powerList b = lazyListBuilder 1 (multiplication b)

operatorIntegerPower :: Double -> Int -> Double
operatorIntegerPower b i = (powerList b) !! i

intPow = operatorIntegerPower
