module UNTESTED.OperatorDoublePipe2 where

import InDyModel
import Compositions

import UNTESTED.OperatorExpandOperatorReturn

-- Pipe2, but with functions with 2 inputs -- untested in-game
operatorDoublePipe2 :: (a -> b -> c) -> (a -> b -> d) -> (c -> d -> e) -> (a -> b -> e)
operatorDoublePipe2 = pipe pipe2 (pipe (apply flipPipe flipPipe) (apply (flip (apply expOpRes flip)) liftM2))
doublePipe2 = operatorDoublePipe2
