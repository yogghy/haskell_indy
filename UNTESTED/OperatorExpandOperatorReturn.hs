module UNTESTED.OperatorExpandOperatorReturn where

import InDyModel
import InDyLibrary
import Compositions

--------------------------------------------------------------------------------
-- expOpRes dev file
--------------------------------------------------------------------------------
-- "Expands the Operator Result" of the supplied function, resulting in an
-- additional input to the input function and 1 less input on the return type.
--------------------------------------------------------------------------------
-- Still needs to be tested in-game with functions that have wierder types,
-- esp. ones that return other functions. Behaviour with nested wrapped
-- functions in the return (e.g. return type :: (a -> (b -> (c -> d)))) is not
-- confirmed either, although I [met4000] think that it correctly unwraps the
-- outer input of the return function.
--------------------------------------------------------------------------------

operatorExpandOperatorResult :: (a -> (b -> c)) -> (a -> b -> c)
operatorExpandOperatorResult = apply flipPipe apply

expOpRes = operatorExpandOperatorResult
