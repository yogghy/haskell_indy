module UNTESTED.OperatorItemItemListCount where

import InDyModel

import Data.List
import UNTESTED.Item

--------------------------------------------------------------------------------
-- Haskell model of the in-game function by the same name.
--------------------------------------------------------------------------------

ilc_1 :: Item -> Int -> Item -> Int
ilc_1 (Item mn _) t (Item cn s) = if mn == cn
    then t + s
    else t

operatorItemItemListCount :: [Item] -> Item -> Int
operatorItemItemListCount l i = Data.List.foldl (ilc_1 i) 0 l
itemListCount = operatorItemItemListCount
