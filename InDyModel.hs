module InDyModel where

import Data.Typeable
import Data.List
import Data.Text

------------------------------------------------------
--   Simulation of ID functional model in Haskell   --
------------------------------------------------------

------------------------------------------------------
--                    Basic things                  --
------------------------------------------------------

-- String contains : Substring -> String -> Bool
operatorStringContains :: Text -> String -> Bool
operatorStringContains x y = Data.Text.isInfixOf x (Data.Text.pack y)

stringContains = operatorStringContains

-- Operator k (constant): return copy of the first argument
operatorConst :: a -> b -> a
operatorConst x y = x

k = operatorConst

-- Arithmetic addition
operatorAdd :: (Num a) => a -> a -> a
operatorAdd a b = a + b

add = operatorAdd

-- Arithmetic subtraction
operatorSubtract :: (Num a) => a -> a -> a
operatorSubtract a b = a - b

subtract = operatorSubtract

-- Equals
operatorEquals :: (Eq a) => a -> a -> Bool
operatorEquals a b = a == b

-- Bool disjunction
operatorOr :: Bool -> Bool -> Bool
operatorOr a b = a || b

or = operatorOr

-- Choice operator
operatorChoice :: Bool -> a -> a -> a
operatorChoice x a b = if x then a else b

choice = operatorChoice

-- Identity operator
operatorId :: a -> a
operatorId a = a

------------------------------------------------------
--                    List things                   --
------------------------------------------------------

-- List contains predicate : List -> Operator -> Bool
operatorListContainsPredicate :: [a] -> (a -> Bool) -> Bool
operatorListContainsPredicate x op = Data.List.any op x

-- Filter list by predicate : Operator -> List -> List
operatorFilter :: (a -> Bool) -> [a] -> [a]
operatorFilter = Data.List.filter

filter = operatorFilter

-- Map : Operator -> List -> List
operatorMap :: (a -> b) -> [a] -> [b]
operatorMap f x = Data.List.map f x
-- map is a common name

-- Reduce: Operator -> List -> Any -> Any
operatorReduce :: (b -> a -> b) -> [a] -> b -> b
operatorReduce op seq val = Data.List.foldl op val seq

reduce   = operatorReduce

operatorReduce1 :: (a -> a -> a) -> [a] -> a
operatorReduce1 op seq = reduce op (Data.List.tail seq) (Data.List.head seq)

reduce1 = operatorReduce1

operatorHead :: [a] -> a
operatorHead = Data.List.head

operatorTail :: [a] -> [a]
operatorTail = Data.List.tail

------------------------------------------------------
--                    Operator things               --
------------------------------------------------------

-- Apply: Operator -> Any -> Any
operatorApply :: (a -> b) -> a -> b
operatorApply f a = f(a)

apply    = operatorApply

-- Apply 2 arguments
operatorApply2 :: (a -> b -> c) -> a -> b -> c
operatorApply2 op a b = op a b

apply2   = operatorApply2

-- Apply 3 arguments
operatorApply3 :: (a -> b -> c -> d) -> a -> b -> c -> d
operatorApply3 op a b c = op a b c

apply3   = operatorApply3

-- Operator disjunction
operatorDisjunction :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
operatorDisjunction x y a = (x a) || (y a)

disj = operatorDisjunction

-- Operator conjunction
operatorConjunction :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
operatorConjunction x y a = (x a) && (y a)

conj = operatorConjunction

-- Pipe or function composition
operatorPipe :: (a -> b) -> (b -> c) -> (a -> c)
operatorPipe f1 f2 x = f2 (f1 x)

pipe = operatorPipe

-- Pipe 2 arguments
-- gives its input to the first and second operators, 
-- and pipes the outputs from both of them to the third operator.
operatorPipe2 :: (a -> b) -> (a -> c) -> (b -> c -> d) -> (a -> d)
operatorPipe2 op1 op2 op3 x = op3 (op1 x) (op2 x)

pipe2 = operatorPipe2
