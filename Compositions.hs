module Compositions where

import InDyModel
import InDyLibrary

--------------------------------------------------------------
--             This file is a WIP                           --
--  Treat all functions here with a grain of salt!          --
--  In-game signatures are quite different fron Haskell's!  --
--------------------------------------------------------------

flipPipe = flip pipe

flipFlip :: b -> (a -> b -> c) -> a -> c
flipFlip = flip flip

applyTo_2 :: (a -> b -> c) -> b -> (a -> c)
applyTo_2 = pipe flip apply

(.:) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(.:) = pipe flipPipe flipPipe

(.::) :: (d -> e) -> (a -> b -> c -> d) -> a -> b -> c -> e
(.::) = pipe (.:) flipPipe

rotate_2_3 :: (a -> b -> c -> d) -> (a -> (c -> b -> d))
rotate_2_3 = apply flipPipe flip

-- Haskell Library composition robinstar
robinstar :: (a -> b -> c -> d) -> c -> a -> b -> d
robinstar = flip . (flip .)

rotate_3_1_2 :: (a -> b -> c -> d) -> (c -> a -> b -> d)
rotate_3_1_2 = apply rotate_2_3 rotate_2_3

-- Rotated pipe2 - a.k.a. liftM2
liftM2 :: (b -> c -> d) -> (a -> b) -> (a -> c) -> a -> d
liftM2 = apply rotate_3_1_2 pipe2

-- Haskell `ap` for operators
ap :: (a -> b -> c) -> (a -> b) -> (a -> c)
ap = apply liftM2 apply

-- Pipe3 attempt
--pipe3 :: (a -> b -> c -> d) -> (x -> a) -> (x -> b) -> (x -> c) -> (x -> d




greater = (>)
greater' = flip greater
greater_10 = greater' 10
my_predicate :: Int -> Bool
my_predicate = greater_10

true_value :: Int -> Int
true_value = (+) 100

false_value :: Int -> Int
false_value x = x + 1


f1 = apply3 liftM2 choice my_predicate true_value
z  = apply3 liftM2 id f1 false_value

q1 = liftM2 choice
q2 = liftM2 id
f1' = apply2 q1 my_predicate true_value
f2' = apply2 q2 f1' false_value

apply2' = flip apply2
f2''    = apply2' f1' q2 false_value

z1 = apply3 liftM2 id (apply3 liftM2 choice my_predicate true_value) false_value

-- pipe3 = ((liftM2 id .) .) . liftM2

--main :: IO()
--main = do
--    print ("hi")
--    print (operatorMap z [1, 2, 20])
