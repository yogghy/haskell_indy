module InDyLibrary where

import InDyModel

-- A function to sum a list of ints.
--
-- Type checker:
-- reduce :: (b -> a -> b) -> [a] -> b -> b
-- add :: Num-> Num -> Num
-- operatorListSumStep1 :: [Num] -> Num - Num
-- operatorListSumStep2 :: Num -> [Num] -> Num
-- operatorListSum :: [Num] -> Num
--
-- We need to bind 1st and 3rd arguments of reduce and leave 2nd free.

operatorListSum :: [Integer] -> Integer
-- Bind 1st
operatorListSumStep1 = apply operatorReduce add
-- Skip 2nd
operatorListSumStep2 = flip operatorListSumStep1
-- Bind 3rd
operatorListSum      = apply operatorListSumStep2 0



-- Operator False: Any -> Bool
operatorFalse :: a -> Bool
operatorFalse x = operatorConst False x

-- Operator True: Any -> Bool
operatorTrue :: a -> Bool
operatorTrue x = operatorConst True x
