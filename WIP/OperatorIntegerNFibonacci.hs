module WIP.OperatorIntegerNFibonacci where

import InDyModel
import InDyLibrary
import Compositions

import Data.List

import UNTESTED.OperatorListGetOrDefault
import UNTESTED.OperatorListLazyListBuilder

--------------------------------------------------------------------------------
-- operatorIntegerNFibonacci dev file
--------------------------------------------------------------------------------
-- Accepts an integer `n`, and returns the `n`th fibonnaci number (starting
-- at [1,1,2,...] such that `n=0`=>`1`, `n=1`=>`1`, ...)
--------------------------------------------------------------------------------
-- TODO: Convert to InDy code
-- note: this code was made to demonstrate an algorithm, not to be good
--------------------------------------------------------------------------------

-- returns a list with the next fibonacci number appended
constructionOp :: [Integer] -> [Integer]
constructionOp b = [head (tail b), (head b) + (head (tail b))]

operatorIntegerNFibonacci :: Int -> Integer
operatorIntegerNFibonacci n = head ((lazyListBuilder [0, 1] constructionOp) !! n)

nFibonacci = operatorIntegerNFibonacci


main :: IO()
main = do
    print (nFibonacci 0)  -- should be: 0
    print (nFibonacci 1)  -- should be: 1
    print (nFibonacci 2)  -- should be: 1
    print (nFibonacci 3)  -- should be: 2
    print (nFibonacci 4)  -- should be: 3
    print (nFibonacci 5)  -- should be: 5

    print (nFibonacci 10) -- should be: 55
