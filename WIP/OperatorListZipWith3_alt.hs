module WIP.OperatorListZipWith3 where

import InDyModel
import InDyLibrary
import Compositions

import Data.Typeable
import Data.List

import WIP.OperatorListZipWith

--------------------------------------------------------------------------------
-- WIP
--------------------------------------------------------------------------------

pipe_3 :: (a -> b -> c -> d) -> (d -> e) -> (a -> b -> c -> e)
pipe_3 f g = \a -> \b -> \c -> g (f a b c)

operatorListZipWith3 :: (l1 -> l2 -> l3 -> l) -> [l1] -> [l2] -> [l3] -> [l]
operatorListZipWith3 = pipe_3 operatorListZipWith (operatorListZipWith apply)


main :: IO ()

l_a = [1.0, 2.0, 2.0]
l_b = [2.0, 2.0, 3.0]
l_c = [1.0, 1.0, 2.0]

main = do
  print l_a
  print l_b
  print l_c
  print (operatorListZipWith3 (\a -> \b -> \c -> (a + b * c)) l_a l_b l_c)
  