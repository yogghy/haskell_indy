module WIP.OperatorListZipWith3 where

import InDyModel
import InDyLibrary
import Compositions

import Data.Typeable
import Data.List

import WIP.OperatorListZipWith3_alt

--------------------------------------------------------------------------------
-- WIP
--------------------------------------------------------------------------------

pipe_4 :: (a -> b -> c -> d -> e) -> (e -> f) -> (a -> b -> c -> f)
pipe_4 f g = \a -> \b -> \c -> \d -> g (f a b c d)

operatorListZipWith4 :: (l1 -> l2 -> l3 -> l) -> [l1] -> [l2] -> [l3] -> [l]
operatorListZipWith4 = pipe_4 operatorListZipWith3 (operatorListZipWith apply)


main :: IO ()

l_a = [1.0, 2.0, 2.0]
l_b = [3.0, 6.0, 2.0]
l_c = [2.0, 2.0, 3.0]
l_d = [1.0, 1.0, 2.0]

main = do
  print l_a
  print l_b
  print l_c
  print l_d
  print (operatorListZipWith4 (\a -> \b -> \c -> \d -> (a + b + c * d)) l_a l_b l_c l_d)
  