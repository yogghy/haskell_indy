module WIP.OperatorDoubleSqrt where

import InDyModel
import Compositions

import UNTESTED.OperatorListLazyListBuilder

import Data.List

--------------------------------------------------------------------------------
-- operatorDoubleSqrt dev file
--------------------------------------------------------------------------------
-- (desc)
--------------------------------------------------------------------------------
-- TODO: Generalise. Solve. Make and test in-game.
--------------------------------------------------------------------------------

-- modulo :: Integer -> Integer -> Integer
-- modulo n m = if n < 0
--     then modulo (n + m) m
--     else if n >= m
--         then modulo (n - m) m
--         else n

operatorDivision :: Double -> Double -> Double
operatorDivision a b = a / b
division = operatorDivision

operatorMultiplication :: Double -> Double -> Double
operatorMultiplication a b = a * b
multiplication = operatorMultiplication

dAdd :: Double -> Double -> Double
dAdd a b = a + b


-- can be replaced with something else more complex to provide better results; doesn't need to be very accurate
rootApprox :: Double -> Double
rootApprox = apply operatorConst 2

newtonRootSequenceBuilder :: Double -> Double -> Double
-- newtonRootSequenceBuilder n r = (r + n / r) / 2
-- newtonRootSequenceBuilder = pipe (pipe (pipe division (ap dAdd)) (division .)) ((flip flip) 2) -- I can't figure out an easy way to get rid of `(division .)`. There might be one though.
-- newtonRootSequenceBuilder n r = r / 2 + n / (2 * r)
newtonRootSequenceBuilder = pipe2 (apply operatorConst (apply (flip division) 2)) (flip (pipe (apply multiplication 2) (flip division))) (apply liftM2 dAdd)

newtonRootSequence :: Double -> [Double]
newtonRootSequence = pipe2 rootApprox newtonRootSequenceBuilder lazyListBuilder


main :: IO()
main = do
    print (take 10 (newtonRootSequence 2))
