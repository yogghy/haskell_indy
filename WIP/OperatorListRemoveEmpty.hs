{-# LANGUAGE OverloadedStrings #-}
module WIP.OperatorListRemoveEmpty where

import InDyModel

import UNTESTED.Nullable
import UNTESTED.Item

import Data.List


-- dev file - removes empty elements from lists


operatorListRemoveEmpty :: (Nullable a) => [a] -> [a]
operatorListRemoveEmpty = apply operatorFilter isNotNull

removeEmpty = operatorListRemoveEmpty


t = print . removeEmpty

main :: IO()
main = do
    t [Item "cobble" 1, Item "cobble" 2, Item "cobble" 3]
    t [Item "cobble" 1, Item "cobble" 2, Item "cobble" 3, nullv, nullv, nullv]
    t [Item "cobble" 1, nullv, Item "cobble" 2, Item "cobble" 3, nullv, nullv]
