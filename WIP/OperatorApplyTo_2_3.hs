module WIP.OperatorApplyTo_2_3 where

import InDyModel
import InDyLibrary
import Compositions

import UNTESTED.OperatorPipeTo_2Input

--------------------------------------------------------------------------------
-- operatorApplyTo_2_3 dev file
--------------------------------------------------------------------------------
-- The same as `operatorApply2`, but applies to the 2nd and 3rd inputs instead
-- of the 1st and 2nd ones. Should return a function with equivalent signature
-- to the input function, missing the 2nd and 3rd arguments (i.e. no wrapping).
--------------------------------------------------------------------------------
-- WIP - dependant on a functional `operatorPipeTo_2` function before it can
-- be completed
--------------------------------------------------------------------------------

-- this is enough for applyTo_2_3 in Haskell...
t0 :: (a -> b -> c -> d) -> (b -> (c -> a -> d))
t0 = pipe flip rotate_2_3

t1 :: (a -> b -> c -> d) -> b -> (c -> a -> d)
t1 = pipe t0 apply

t2 :: b -> (a -> b -> c -> d) -> (c -> a -> d)
t2 = flip t1

t3 :: b -> (a -> b -> c -> d) -> c -> (a -> d)
t3 = pipe_2 t2 apply

operatorApplyTo_2_3 :: (a -> b -> c -> d) -> b -> c -> (a -> d)
operatorApplyTo_2_3 = flip t3
--apply_2_3 = flip (pipe_2 (flip (pipe (pipe flip rotate_2_3) apply)) apply)


main :: IO ()
main = do
    print ""
