module WIP.OperatorListFindIndex where

import InDyModel

import Data.Typeable
import Data.List
import Data.Text

import UNTESTED.OperatorExpandOperatorReturn

--------------------------------------------------------------------------------
-- WIP
--------------------------------------------------------------------------------

flipPipe :: (b -> c) -> (a -> b) -> (a -> c)
flipPipe = flip pipe

rotate_2_3 :: (a -> b -> c -> d) -> a -> c -> b -> d
rotate_2_3 = apply flipPipe flip

apply_2_3 :: (z -> a -> b -> c) -> a -> (b -> z -> c)
apply_2_3 = pipe flip rotate_2_3

join :: (a -> a -> b) -> a -> b
join = apply2 pipe2 id id

-- because Haskell doesn't like `-1`
neg1 = 0 - 1


-- Making the operator:

ret1 :: Integer -> Bool -> Integer
ret1 = k


t1 :: Integer -> Integer
t1 = apply (-) neg1

t2 :: Integer -> Integer
t2 = apply (+) neg1   -- equivalent to `decrement`

t3 :: Bool -> (Integer -> Integer)
t3 = apply3 apply_2_3 choice t1 t2

t3_2 :: Bool -> Integer -> Integer
t3_2 = apply expOpRes t3

ret2 :: Integer -> Bool -> Integer
ret2 = flip t3_2


t4 :: Integer -> Bool
t4 = apply (<) neg1

t5 :: Bool -> (Integer -> Bool -> Integer)
t5 = apply3 apply_2_3 choice ret1 ret2

t6 :: Integer -> (Integer -> Bool -> Integer)
t6 = pipe t4 t5

t6_2 :: Integer -> Integer -> (Bool -> Integer)
t6_2 = apply expOpRes t6

reduceOp :: Integer -> (Bool -> Integer)
reduceOp = apply join t6_2


-- makes errors ig
t7 :: (b -> a -> b) -> (b -> [a] -> b)
t7 = apply rotate_2_3 reduce

findIndexBool :: [Bool] -> Integer
findIndexBool = apply2 t7 reduceOp neg1
findBy_b = findIndexBool

-- t7_1 :: [Bool] -> Integer -> Integer
-- t7_1 = apply reduce reduceOp

-- t7_2 :: Integer -> [Bool] -> Integer
-- t7_2 = flip t7_1

-- findIndexBool :: [Bool] -> Integer
-- findIndexBool = apply t7_2 neg1
-- findBy_b = findIndexBool


t8 :: [a] -> (a -> b) -> [b]
t8 = flip operatorMap

t9 :: (a -> [Bool]) -> (a -> Integer)
t9 = apply flipPipe findIndexBool

findIndexPredicate :: [a] -> (a -> Bool) -> Integer
findIndexPredicate = pipe t8 t9
findBy_p = findIndexPredicate


t10 :: (a -> Bool) -> [a] -> Integer
t10 = flip findIndexPredicate

t11 :: (Eq a) => a -> [a] -> Integer
t11 = pipe (==) t10


findIndexEquals :: (Eq a) => [a] -> a -> Integer
findIndexEquals = flip t11
findBy_eq = findIndexEquals


l_1 :: [Bool]
l_1 = [False, False, True, False, True]

l_2 :: [Int]
l_2 = [1, 2, 2, 3, 1]

main :: IO ()
main = do
    print (l_1)
    print (findBy_b l_1)
    print (l_2)
    print (findBy_eq l_2 3)
