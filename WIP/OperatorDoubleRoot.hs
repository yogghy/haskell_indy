module WIP.OperatorDoubleRoot where

import InDyModel
import Compositions

import UNTESTED.OperatorDoublePipe2
import UNTESTED.OperatorIntegerPower
import UNTESTED.OperatorListLazyListBuilder
import UNTESTED.OperatorTriplePipe2

--------------------------------------------------------------------------------
-- doubleRoot dev file
--------------------------------------------------------------------------------
-- Returns the (2nd input)th root of the first input, or the first input to the
-- power of the inverse of the 2nd input.
--------------------------------------------------------------------------------
-- TODO: Make and test ingame
--------------------------------------------------------------------------------

dAdd :: Double -> Double -> Double
dAdd a b = a + b

dSub :: Double -> Double -> Double
dSub a b = a - b

operatorDivision :: Double -> Double -> Double
operatorDivision a b = a / b
division = operatorDivision

-- can be replaced with something else more complex to provide better results; doesn't need to be very accurate
rootApprox :: Double -> Int -> Double
rootApprox = pipe (apply operatorConst 2) operatorConst

nrsb1_0 :: Double -> Double -> Double -> Double
-- nrsb0 n p r = multiplication (division (dSub p 1) p) r
nrsb1_0 = const (pipe (pipe2 ((flip dSub) 1) operatorId division) multiplication)

nrsb1 :: Double -> Int -> Double -> Double
-- nrsb1 n p r = nrsb1_0 n (fromIntegral p) r
nrsb1 = pipe nrsb1_0 (apply pipe fromIntegral)

nrsb2_0 :: Int -> Double -> Double
-- nrsb2_0 p r = multiplication (fromIntegral p) (intPow r (fromIntegral (InDyModel.subtract (fromIntegral p) 1)))
nrsb2_0 = apply2 ap (pipe fromIntegral (pipe multiplication flipPipe)) (pipe fromIntegral (pipe (flip InDyModel.subtract 1) (pipe fromIntegral (flip intPow))))

nrsb2 :: Double -> Int -> Double -> Double
-- nrsb2 n p r = division n (nrsb2_0 p r)
nrsb2 = pipe division (pipe flipPipe (apply pipe nrsb2_0))

newtonRootSequenceBuilder :: Double -> Int -> Double -> Double
-- newtonRootSequenceBuilder n p r = r - (division ((intPow r p) - n) (multiplication (fromIntegral p) (intPow r (p - 1))))
newtonRootSequenceBuilder = apply3 triplePipe2 nrsb1 nrsb2 dAdd

newtonRootSequence :: Double -> Int -> [Double]
newtonRootSequence = apply3 doublePipe2 rootApprox newtonRootSequenceBuilder lazyListBuilder

main :: IO()
main = do
    print (take 10 (newtonRootSequence 3.375 3))
