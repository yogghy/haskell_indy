{-# LANGUAGE OverloadedStrings #-}
module WIP.OperatorListItemListCount where

import InDyModel
import Compositions

import UNTESTED.OperatorItemItemListCount
import UNTESTED.OperatorListUniq
import UNTESTED.OperatorPipeTo_2Input

import Data.Text
import Data.List
import UNTESTED.Item

--------------------------------------------------------------------------------
-- operatorListItemListCount dev file
--------------------------------------------------------------------------------
-- Takes a list of items and returns a list of items, merging items that have
-- the same name into the same stack.
--------------------------------------------------------------------------------
-- TODO: Needs to be made in-game and tested.
--------------------------------------------------------------------------------

inventoryList :: [Item]
inventoryList = [Item "cobble" 2, Item "cobble" 3, Item "stone" 3, Item "cobble" 1]

mapOp :: [Item] -> Item -> Item
mapOp = pipe itemListCount (apply ap withSize)

itemCount :: [Item] -> [Item]
itemCount = pipe2 mapOp operatorId operatorMap

operatorListItemListCount :: [Item] -> [Item]
operatorListItemListCount = pipe itemCount uniq


main :: IO()
main = do
    print inventoryList
    print (operatorListItemListCount inventoryList)
