module WIP.OperatorListZipWith3 where

import InDyModel
import InDyLibrary
import Compositions

import Data.Typeable
import Data.List

import WIP.OperatorListZipWith

--------------------------------------------------------------------------------
-- WIP
--------------------------------------------------------------------------------

pipe3 :: (a -> b) -> (a -> c) -> (a -> d) -> (b -> c -> d -> e) -> (a -> e)
pipe3 f1 f2 f3 g = \x -> g (f1 x) (f2 x) (f3 x)

pipe_4 :: (a -> b -> c -> d -> e) -> (e -> f) -> (a -> b -> c -> d -> f)
pipe_4 f g = \a -> \b -> \c -> \d -> g (f a b c d)

---------

rotate_4_1_2_3 f d a b c = f a b c d

rotate_2_3_1 = pipe rotate_3_1_2 rotate_3_1_2

ro3_f :: (l1 -> l2 -> l3 -> l) -> [l1] -> [l2] -> [l3] -> Int -> l
ro3_f = rotate_4_1_2_3 (rotate_2_3_1 (pipe get (rotate_3_1_2 (flip (pipe get (flip (pipe get pipe3)))))))

reduceOp3 :: (l1 -> l2 -> l3 -> l) -> [l1] -> [l2] -> [l3] -> [l] -> a -> [l]
reduceOp3 = pipe_4 (pipe_4 (pipe_4 ro3_f (pipe listLength)) (flip (pipe2 id) append)) (flip pipe k)

operatorListZipWith3 :: (l1 -> l2 -> l3 -> l) -> [l1] -> [l2] -> [l3] -> [l]
operatorListZipWith3 = pipe (flip (pipe (flip (pipe (flip reduce) flip) []) (pipe_4 reduceOp3))) (pipe2 id id)


main :: IO ()

l_a = [1.0, 2.0, 2.0]
l_b = [2.0, 2.0, 3.0]
l_c = [1.0, 1.0, 2.0]

main = do
  print l_a
  print l_b
  print l_c
  print (operatorListZipWith3 (\a -> \b -> \c -> (a + b * c)) l_a l_b l_c)
  