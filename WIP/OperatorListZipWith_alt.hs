module WIP.OperatorListZipWith where

import InDyModel
import InDyLibrary
import Compositions

import Data.Typeable
import Data.List

--------------------------------------------------------------------------------
-- WIP
--------------------------------------------------------------------------------

operatorAppend :: [a] -> a -> [a]
operatorAppend l v = l ++ [v]
append = operatorAppend

operatorGet :: [a] -> Int -> a
operatorGet l i = l !! i
get = operatorGet

operatorListLength :: [a] -> Int
operatorListLength = Data.List.length
listLength = operatorListLength

pipe_2 :: (a -> b -> c) -> (c -> d) -> (a -> b -> d)
pipe_2 f g = \a -> \b -> g (f a b)

pipe_3 :: (a -> b -> c -> d) -> (d -> e) -> (a -> b -> c -> e)
pipe_3 f g = \a -> \b -> \c -> g (f a b c)

pipe2_2 :: (a -> b -> c) -> (a -> b -> d) -> (c -> d -> e) -> (a -> b -> e)
pipe2_2 f1 f2 g = \a -> \b -> g (f1 a b) (f2 a b)

rotate_4_1_2_3 f d a b c = f a b c d

join = pipe2 id id


func :: Integer -> Integer -> Integer
func a b = a * 2 + b

l_1 :: [Integer]
l_1 = [1, 2, 3, 5]

l_2 :: [Integer]
l_2 = [2, 3, 4, 1]


ro_f :: (a -> b -> c) -> [a] -> [b] -> Int -> c
ro_f = rotate_3_1_2 (flip (pipe get (flip (pipe get pipe2))))

reduceOp :: (a -> b -> c) -> [a] -> [b] -> [c] -> z -> [c]
reduceOp = pipe_3 (pipe_3 (pipe_3 ro_f (pipe listLength)) (flip (pipe2 id) append)) (flip pipe k)

getSmallestList :: [z] -> [z] -> [z]
getSmallestList = join (rotate_3_1_2 (rotate_3_1_2 (join (rotate_4_1_2_3 (flip (pipe_2 (pipe length (flip (pipe length (>)))) choice))))))

-- operatorListZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
operatorListZipWith = flip (flip (pipe reduceOp pipe2_2) getSmallestList) (rotate_3_1_2 reduce [])


main :: IO ()

main = do
  print (l_1)
  print (l_2)
  print (operatorListZipWith func l_1 l_2)

  print ([1.0, 2.0, 2.0])
  print ([2.0, 2.0, 3.0])
  print (operatorListZipWith (\a -> \b -> (a / b)) [1.0, 2.0, 2.0] [2.0, 2.0, 3.0])
  