import InDyModel
import InDyLibrary
import Data.Typeable
import Data.List


-- Simulation of Thaumcraft Jar
data Jar = Jar {
    essentiaType :: String,
    amount :: Integer
} deriving (Show)

-- Simulated operator to get essentia type from Jar.
-- In game it will be NBT.String("essentiaType")
operatorEssentiaType :: Jar -> String
operatorEssentiaType x = essentiaType x

-- Simulated operator to get essentia amount from Jar.
-- In game it will be NBT.Integer("amount")
operatorEssentiaAmount :: Jar -> Integer
operatorEssentiaAmount x = amount x

------------------------------------------------------
--                    Test wall of jars             --
------------------------------------------------------

testJar = Jar "haskellium" 42
testJar2 = Jar "herba" 42

-- In game this will be an array of Block Readers with TileEntityNBT aspect
testJars = [
    Jar "herba" 100,
    Jar "praecantio" 200,
    Jar "instrumentum" 150,
    Jar "herba" 200,
    Jar "" 0
    ]

------------------------------------------------------
--      Operators, operators, operators             --
------------------------------------------------------

-- Does jar have given essentia?
-- Type check:
-- operatorEssentiaType :: Jar -> String
-- op_equals :: a -> a -> Bool
-- operatorEqualsJar :: Jar -> String -> Bool
operatorEqualsJar :: Jar -> String -> Bool
operatorEqualsJar = pipe operatorEssentiaType operatorEquals

flipEqualsJar :: String -> Jar -> Bool
flipEqualsJar = flip operatorEqualsJar

-- Filter list of jars by essentia type
--
-- Type checker:
-- op_filter :: (a -> Bool) -> [a] -> [a]
-- flipEqualsJar :: String -> Jar -> Bool
-- operatorFilterByType :: String -> [Jar] -> [Jar]
operatorFilterByType :: String -> [Jar] -> [Jar]
operatorFilterByType = pipe flipEqualsJar operatorFilter

-- Get essentia amount in the list of jars
operatorGetAmounts :: [Jar] -> [Integer]
operatorGetAmounts = apply map operatorEssentiaAmount

-- Sum of all amounts in a jar list
operatorTotalAmount :: [Jar] -> Integer
operatorTotalAmount = pipe operatorGetAmounts operatorListSum

------------------------------------------------------
--           Bind testJars approach                 --
------------------------------------------------------
flipFilterByType :: [Jar] -> String -> [Jar]
flipFilterByType = flip operatorFilterByType

-- Bind testJars leaving only 1 string argument
-- Filter testJars by essentia type
y1 :: String -> [Jar]
y1 = apply flipFilterByType testJars

-- Get essentia amount by type in testJars
totalAmount :: String -> Integer
totalAmount = pipe y1 operatorTotalAmount

------------------------------------------------------
--                    Final result                  --
------------------------------------------------------
z1 = apply totalAmount "herba"
z2 = apply totalAmount "instrumentum"
z3 = apply totalAmount "haskellium"

------------------------------------------------------
--            Non-binding approach                  --
------------------------------------------------------
flipPipe = flip pipe

-- Second-tier composition
-- See https://stackoverflow.com/a/28739294 for more details

-- #################################################  --
-- Different behavior between InDy and Haskell!
-- Haskell has right-associativity, so a -> (b -> d) is the same as a -> b -> d
-- InDy does not! so the signature shows up as Any -> Operator
-- Fortunately, it works for direct application.
-- #################################################  --
operatorPipeTwoArgs :: (c -> d) -> (a -> b -> c) -> a -> (b -> d)
operatorPipeTwoArgs = pipe flipPipe flipPipe 

-- Resulting operator
q :: String -> [Jar] -> Integer
q = apply2 operatorPipeTwoArgs operatorTotalAmount operatorFilterByType 

------------------------------------------------------
--                    Final result                  --
------------------------------------------------------
q1 = apply2 q "herba"        testJars
q2 = apply2 q "instrumentum" testJars
q3 = apply2 q "haskellium"   testJars

main :: IO()

main = do

    print (z1)
    print (z2)
    print (z3)

    print (q1)
    print (q2)
    print (q3)