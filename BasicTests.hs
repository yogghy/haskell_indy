import InDyModel
import InDyLibrary

import Data.Typeable
import Data.List
import Data.Text

x = apply2 choice False 1
y = apply2 choice True 1

a = pipe2 operatorFalse id choice
    -- if (false(x)) then id(x) else ?
b = pipe2 operatorTrue  id choice

f :: Int -> Int
f x = x * 2

f1 :: Int -> Int
f1 x = x + 1

h = apply pipe f

pred1 :: Int -> Bool
pred1 = (==) 10

pred2 :: Int -> Bool
pred2 = flip(<) 100

func3 = (&&)


main :: IO ()

main = do
    print (x(2))
    print (y(2))
    print (a 2 3)
    print (b 2 3)
    print (InDyModel.or True True)

    print (apply2 h f1 2)