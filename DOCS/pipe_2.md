# Pipe To 2 Inputs (pipe_2, pipeTo_2)

## Signature
```haskell
-- actual:
pipe_2 :: (a -> b -> c) -> (c -> d) -> (a -> b -> d)

-- in-game:
pipe_2 :: Operator -> Any -> Any
```
Note: compare to `pipe`'s signature:
```haskell
pipe :: (a -> c) -> (c -> d) -> (a -> d)
```

## Definition
```haskell
temp1 = pipe(flipPipe, flipPipe)
temp2 = flip(temp1)
temp3 = apply2(temp2, temp2, expOpRes)

pipe_2 = apply(expOpRes, temp3)
```
See: [flipPipe](./flipPipe.md), [expOpRes](./expOpRes.md).

Note: see also the "`flipPipe` method" in the usage section for a recommended alternative to constructing the `pipe_2` operator.

## Example Usage
The standard `pipe` operation applies *1* input of the first operator. `pipe_2` applies *2* inputs. For example, piping two `add`s together;
```haskell
add :: Number -> Number -> Number

-- using `pipe`:
add3Nums :: Number -> Number -> Number -- in-game signature
add3Nums = pipe(add, add)
-- note: attempting to use this operator will result in
--   "The operator addition received an input with type
--    Operator at position 1 while the type Number was expected"

-- using `pipe_2`:
add3Nums :: Number -> Number -> Number -> Number
add3Nums = apply2(pipe_2, add, add)

-- using `flipPipe` method for `pipe_2` (see below):
temp = apply(flipPipe, add)

add3Nums :: Number -> Number -> Number -> Number
add3Nums = pipe(add, temp)
```

A (recommended) alternative to constructing the `pipe_2` operator is the "`flipPipe` method":
```haskell
f :: a -> b -> c
g :: c -> d

temp = apply(flipPipe, g)

final :: a -> b -> d
final = pipe(f, temp)
```
See: [flipPipe](./flipPipe.md).
