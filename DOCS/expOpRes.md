# Expand Operator Result (expOpRes)

## Signature
```haskell
-- actual:
expOpRes :: (a -> (b -> c)) -> (a -> b -> c)

-- in-game:
expOpRes :: Operator -> Operator
```

## Definition
```haskell
expOpRes = apply(flipPipe, apply)
```
See: [flipPipe](./flipPipe.md).

Note: the following, simpler, definition may work in most situations;
```haskell
expOpRes = apply2
```

## Example Usage
Some specific operations construct operators with the signature `Any -> Operator`, even though the operator has multiple inputs. `expOpRes` can be used to "expose" the other inputs, allowing for e.g. operations such as `flip` that would otherwise throw "The operator Flip received the operator [...] with input length 1 while length 2 is required".
<!-- TODO: add specific example -->
```haskell
f :: Any -> Operator

f_expanded :: Any -> Any -> Any
f_expanded = apply(expOpRes, f)
```

more specifically;
```haskell
g :: a -> (b -> c)

g_expanded :: a -> b -> c
g_expanded = apply(expOpRes, g)
```
