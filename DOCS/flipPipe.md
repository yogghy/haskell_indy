# flipPipe

## Signature
```haskell
-- actual:
flipPipe :: (b -> c) -> (a -> b) -> (a -> c)

-- in-game:
flipPipe :: Operator -> Operator -> Operator
```

## Definition
```haskell
flipPipe = flip(pipe)
```

## Example Usage
Typically used in the composition of other operators.
