import InDyModel
import InDyLibrary

import Data.Typeable
import Data.List
import Data.Text

------------------------------------------------------
--  Item Sorter by Oredictionary Name               --
------------------------------------------------------

-- Item definition that we are interested in
data Item = Item {
        name :: String,
        oredictNames :: [String]
    } deriving (Show)

-- Oredictionary names for Item
operatorOredictNames :: Item -> [String]
operatorOredictNames x = oredictNames x

-- Plain name for Item
operatorName :: Item -> String
operatorName x = name x


------------------------------------------------------
--                    Test "chest"                  --
------------------------------------------------------

testItem1 = Item "haskellOre" ["testOre", "anyOre"]

testItems = [
        Item "haskellWood" ["testWood", "anyWood"],
        Item "Golden Chestplate" ["Golden Chestplate"],
        Item "junk" ["junk"],
        Item "Haskell Boots" ["Haskell Boots", "anyBoots"],
        Item "nevermOre" ["anyOre"]
        ]

------------------------------------------------------
--        Strings to define for each sorter         --
------------------------------------------------------

desiredItems = [
    pack("Ore"), 
    pack("Golden")
    ]


-- This section is present only for clarity and better understanding!

{- Scalar operations : reduce(or, [Bool], False)

op_r1 =                     apply  reduce or
op_r2 =                     flip   op_r1
op_r3 =                     apply  op_r2  False
op_or_list = op_r3

-}

-- Now the real thing: reduce list of operators to a single operator!
-- Operator operations: reduce(disj, [a-> Bool], k(False))


operatorReduceStep1 = apply reduce disj
operatorReduceStep2 = flip  operatorReduceStep1
operatorReduceStep3 = apply operatorReduceStep2 operatorFalse
operatorOrList = operatorReduceStep3


-- ################################    Actions    ################################

listPredicates :: [String -> Bool]                                -- [substring contained in string]
listPredicates = operatorMap operatorStringContains desiredItems

reducedListPredicates :: String -> Bool                          -- OR on all substrings
reducedListPredicates = apply operatorOrList listPredicates

-- At last something directly usable!
operatorStringIsGood :: String -> Bool
operatorStringIsGood = reducedListPredicates

-- More intermediate magic

p1 :: Item -> (String->Bool) -> Bool
--   operatorOredictNames   |: Item -> [String]
--   op_list_contains_p :: [a] -> (a -> Bool) -> Bool    List -> Op -> Bool
p1 = pipe operatorOredictNames operatorListContainsPredicate

flipP1 :: (String -> Bool) -> Item -> Bool
flipP1 = flip p1  

-------------------------------------------------------------------
--                                                               --
--                                                               --
--                        THE FINAL THING                        --
--                                                               --
--                                                               --
-------------------------------------------------------------------
operatorItemPredicate :: Item -> Bool
operatorItemPredicate = apply flipP1 operatorStringIsGood

-- Test!
x = operatorMap operatorItemPredicate  testItems 

main :: IO ()

main = do
    mapM_ print (testItems)
    putStrLn("")
    print (desiredItems)
    putStrLn("")
    print (x)